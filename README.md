# INSA's Room Managment

This project aims to simulate an automatic managment of INSA's classrooms.

## Clone project

To test our project, you need to clone all the repositories present in this Bitbucket project. These repo are : 

* OM2M architecture : https://bitbucket.org/irmgeilz/irm_om2m
* Orchestrator : https://bitbucket.org/irmgeilz/irm_service_orchestrator
* Generator : https://bitbucket.org/irmgeilz/irm_service_generator
* LightManager : https://bitbucket.org/irmgeilz/irm_service_lightmanager
* AlarmManager : https://bitbucket.org/irmgeilz/irm_service_alarmmanager
* HeaterManager : https://bitbucket.org/irmgeilz/irm_service_heatermanager
* DoorManager : https://bitbucket.org/irmgeilz/irm_service_doormanager
* WindowManager : https://bitbucket.org/irmgeilz/irm_service_windowmanager
* PresenceManager : https://bitbucket.org/irmgeilz/irm_service_presencemanager
* Node-red dashboard : https://bitbucket.org/irmgeilz/irm_service_nodered

## Install project

To install our project, after cloning all the repo mentionned above, you need to import in Eclipse as Maven project all the services : 

* Orchestrator
* Generator
* LightManager
* AlarmManager
* HeaterManager
* DoorManager
* WindowManager
* PresenceManager

Then import the Node-Red Dashboard into Node-red

## Run project

To run our project, you need to :

* Start the 3 CSE of OM2M (in-gei, mn-room_01, mn-room_02)
* Start all the micro-services
* Start the dashboard on Node-red

Then on the dashboard you'll see a button to generate the OM2M architecture and populate it. After generation, a dashboard will appear. You will be able to interact with the rooms and observe the differents scenarios.

## If you find any trouble installing the project

Please mail us at one of these adresses :

* lautredo@etud.insa-toulouse.fr
* zanchi@etud.insa-toulouse.fr
